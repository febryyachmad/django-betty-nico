# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from django.http import HttpResponse

def IndexViews(request, extra_context={}):
	extra_context.update({'title':'Selamat Datang'})
	return render(request, 'frontend/index.html', extra_context)